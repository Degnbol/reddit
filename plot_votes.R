#!/usr/bin/env Rscript

library(data.table)
library(ggplot2)


votes = fread("votes.tsv")
votes = merge(votes, votes[,.(frac=1/.N),by=id], by="id")
# Good. no yes AND no sentences:
nrow(votes[(yes==T) && (no==T)])
nrow(votes[(yes==F) && (no==F)])
# always either yes OR no
votes[,vote:=yes]
votes[,no:=NULL]

summ = data.frame()
for(neg in c(T, F)) {
    for(vot in c(T, F)) {
        summ = rbind(summ,
            votes[(vote==vot) & (negate==neg), .(frac=T, score=F, vote=vot, negate=neg, count=sum(frac))],
            votes[(vote==vot) & (negate==neg), sum(frac), by=id][V1==1, .(frac=F, score=F, vote=vot, negate=neg, count=sum(V1))],
            votes[(vote==vot) & (negate==neg), .(frac=T, score=T, vote=vot, negate=neg, count=sum(frac*score))],
            votes[(vote==vot) & (negate==neg), sum(frac*score), by=id][V1==floor(V1), .(frac=F, score=T, vote=vot, negate=neg, count=sum(V1))]
        )
    }
}

# we only add ~5 percent of count data by considering frac, so only use comments without ambiguity
summ[, sum(count), by=.(score, frac)]
summ = summ[frac==F]
summ[, frac:=NULL]
# use negation
summ[negate==T, vote:=!vote]
summ[, vote:=fifelse(vote, "vote yes", "vote no")]

ylimit = summ[,sum(count), by=.(vote, score)][,max(V1)] * 1.05
ggplot(summ, aes(x=score, y=count, fill=negate)) +
    geom_col(position=position_stack(reverse=T)) +
    xlab("weighted by comment score") +
    scale_y_continuous(expand=c(0,0), limits=c(0, ylimit)) +
    theme_linedraw() +
    scale_fill_manual(values=c("gray", "maroon")) +
    guides(fill=guide_legend(title="negation")) +
    facet_grid(cols=vars(vote)) +
    theme(panel.grid.major.x=element_blank(), panel.grid.major.y=element_line(color="gray"), panel.grid.minor.y=element_line(color="lightgray")) +
    ggtitle("r/2007scape vote commenting")

ggsave("votes_bar.pdf", width=5, height=6)
ggsave("votes_bar.png", width=5, height=6)


