#!/usr/bin/env python3
import praw
import re
import os
import networkx as nx
import numpy as np

def argmax(x, n):
    """
    Get indexes of top n from x
    :param x: collection
    :param n: number of top values returned
    :return: unsorted indexes for top n values in x
    """
    return np.argpartition(x, -n)[-n:]


def write_graphml(fname, G):
    """
    Write graph to graphml which does not support lists so they are converted to string.
    :param fname:
    :param G:
    :return: None
    """
    _G = G.copy()
    for n, d in _G.nodes(data=True):
        for k, vs in d.items():
            if type(vs) in [list, set]:
                d[k] = str(vs)

    for u, v, d in _G.edges(data=True):
        for k, vs in d.items():
            if type(vs) is list: d[k] = str(vs)

    nx.write_graphml(_G, os.path.expanduser(fname))




os.chdir(os.path.expanduser("~/OneDrive/projects/reddit/"))
with open("mostCommon500.txt") as infile:
    mostCommon = set(infile.read().strip().split(" "))
with open("customCommon.txt") as infile:
    mostCommon = mostCommon.union(infile.read().strip().split(" "))


def get_words(text):
    words = re.findall("[A-Za-z0-9\-]+", text)
    words = [w.lower() for w in words]
    words = [w for w in words if w not in mostCommon]
    words = [w for w in words if len(w) > 1]
    return set(words)



reddit = praw.Reddit(client_id='mhuAg6cRsaqYyg', client_secret='S7MgTEHh_0ptKq0iks0jFhZ0qoS0tQ', user_agent='scraper')


posts = []
for s in reddit.subreddit('2007scape').hot(limit=1000):
    s.comments.replace_more(limit=0)
    # 'url', 'created'
    posts.append({
        'id': s.id, 'title': s.title, 'title_words': get_words(s.title), 'score': s.score, 'num_comments': s.num_comments, 'created': s.created,
        'selftext': s.selftext, 'selftext_words': get_words(s.selftext),
        'comments': [{'id': c.id, 'score': c.score, 'body': c.body, 'body_words': get_words(c.body), 'parent': c.parent().id} for c in s.comments.list()],
    })

for p in posts:
    p["comments_dict"] = {c["id"]: c for c in p["comments"]}
    p["comments_dict"][p["id"]] = {'body_words': p["selftext_words"]}

def get_edges(words):
    for w1 in words:
        for w2 in words:
            if w1 != w2:
                yield w1, w2


U_inComment = nx.Graph()
U_inComment.add_nodes_from((w, {'scores': []}) for s in posts for c in s['comments'] for w in c['body_words'])
U_inComment.add_edges_from((u, v, {'scores': []}) for s in posts for c in s['comments'] for u, v in get_edges(c['body_words']))

U_response = nx.DiGraph()
U_response.add_nodes_from((w, {'scores': []}) for s in posts for w in s['selftext_words'])
U_response.add_nodes_from((w, {'scores': []}) for s in posts for c in s['comments'] for w in c['body_words'])
U_response.add_edges_from((u, v, {'scores': []}) for s in posts for c in s['comments'] for u in s["comments_dict"][c['parent']]['body_words'] for v in c['body_words'])

for s in posts:
    for c in s["comments"]:
        for w in c["body_words"]:
            U_inComment.nodes[w]["scores"].append(c["score"])
            U_response.nodes[w]["scores"].append(c["score"])

for s in posts:
    for c in s["comments"]:
        for u, v in get_edges(c["body_words"]):
            U_inComment[u][v]["scores"].append(c["score"])

        for u in s["comments_dict"][c['parent']]['body_words']:
            for v in c['body_words']:
                U_response[u][v]["scores"].append(c["score"])


for G, G_name in zip([U_inComment, U_response], ["inComment", "response"]):
    for n, d in G.nodes(data=True):
        d["len"] = len(d["scores"])
        d["sum"] = np.sum(d["scores"])
        d["mean"] = np.mean(d["scores"])
        d["median"] = np.median(d["scores"])

    for u, v, d in G.edges(data=True):
        d["len"] = len(d["scores"])
        d["sum"] = np.sum(d["scores"])
        d["mean"] = np.mean(d["scores"])
        d["median"] = np.median(d["scores"])

for G, G_name in zip([U_inComment, U_response], ["inComment", "response"]):
    for metric in ["len", "sum"]:
        edges = np.asarray(G.edges, dtype=object)
        edgeMetrics = np.asarray([d[metric] for u, v, d in G.edges(data=True)])
        edgePerm = np.argsort(-edgeMetrics)

        edgesSets = np.asarray([set(e) for e in edges])
        for i in range(1, len(edgesSets)):
            topNodes = set.union(*edgesSets[edgePerm[0:i]])
            if len(topNodes) >= 100: break


        G_top = G.copy()
        G_top.remove_nodes_from(G.nodes - topNodes)

        write_graphml(f"{G_name}_top{metric.capitalize()}s.graphml", G_top)





