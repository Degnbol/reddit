#!/usr/bin/env python3
import re
import pickle
from datetime import datetime
import pandas as pd


with open("vote_comments.pickle", 'rb') as pkl:
    posts = pickle.load(pkl)



def sinceWed(unixtime):
    date = datetime.utcfromtimestamp(int(unixtime))
    return date.year == 2021 and date.month == 9 and date.day >= 15



df = []

for p in posts:
    if not sinceWed(p['created']): continue
    for c in p["comments"]:
        sentences = re.findall("[A-Za-z0-9\-' ]+", c["body"])
        for sent in sentences:
            sent = sent.strip()
            words = sent.split()
            if {"vote", "voted", "voting"}.isdisjoint(words) or {"yes", "no"}.isdisjoint(words): continue
            # ignore down-voted and up-voted
            about_vote = 0
            for i, w in enumerate(words):
                if w in {"vote", "voted", "voting"}:
                    if words[i-1] not in ["down", "up"]: about_vote += 1
            if about_vote == 0: continue

            row_sentence = dict(yes="yes" in words, no="no" in words, negate=not {"not", "don't", "doesn't"}.isdisjoint(words), sentence=sent)
            df.append({**row_sentence, **dict(score=c['score'], id=c['id'], parent=c['parent'])})


pd.DataFrame(df).to_csv("votes.tsv", sep='\t', index=False)


