#!/usr/bin/env python3
import praw
import re
import os
import numpy as np
import pickle

# This file is to guess the outcome of a runescape vote.


def get_words(text):
    words = re.findall("[A-Za-z0-9\-']+", text)
    words = [w.lower() for w in words]
    words = [w for w in words if len(w) > 1]
    return words


reddit = praw.Reddit(client_id='mhuAg6cRsaqYyg', client_secret='S7MgTEHh_0ptKq0iks0jFhZ0qoS0tQ', user_agent='scraper')


posts = []

for s in reddit.subreddit('2007scape').hot(limit=1000):
    print(s.title)
    s.comments.replace_more()

    vote_comments = []
    for c in s.comments.list():
        if not set(get_words(c.body)).isdisjoint({"vote", "voted"}):
            vote_comments.append(dict(id=c.id, score=c.score, body=c.body, body_words=get_words(c.body), parent=c.parent().id))

    if len(vote_comments) > 0:
        posts.append(dict(id=s.id, title=s.title, title_words=get_words(s.title), score=s.score, num_comments=s.num_comments, created=s.created, comments=vote_comments))


with open("vote_comments.pickle", 'wb') as pkl:
    pickle.dump(posts, pkl)


